# The Magic Constant

### Which is the magical number for the numbers with 4 digits?
The magic constant is 6174

### How many steps you need as maximum to reach that number?
I've used a hashmap of <Steps, Number of numbers that require those steps>
to know the distribution of all the possible values and the output was the following:

{ \
    1=384, \
    2=576, \
    3=2400, \
    4=1272, \
    5=1518, \
    6=1656, \
    7=2184 \
}

So, in conclusion, at least we need <b>one</b> step and at most <b>seven</b>.

### how many 4 digit numbers need only 1 step, how many need 2 steps, 3 steps, etc…, until the maximum of the steps?
The question above already answers this.