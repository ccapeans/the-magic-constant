package test;

import com.trileuco.InvalidNumberException;
import com.trileuco.MagicConstant;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MagicConstantTest {

    @Test
    public void validInputNumber() throws InvalidNumberException {
        Integer result = MagicConstant.find("1234");
        assertEquals(6174, result, "Number is invalid");
    }

    @Test
    public void validInputNumberStartingWithZero() throws InvalidNumberException {
        Integer result = MagicConstant.find("0234");
        assertEquals(6174, result, "Number is invalid");
    }

    @Test
    public void validInputNumberEndingWithZero() throws InvalidNumberException {
        Integer result = MagicConstant.find("2640");
        assertEquals(6174, result, "Number is invalid");
    }

    @Test(expected = InvalidNumberException.class)
    public void invalidInputMoreThanFourDigits() throws InvalidNumberException {
        Integer result = MagicConstant.find("26140");
    }

    @Test(expected = InvalidNumberException.class)
    public void invalidInputLessThanFourDigits() throws InvalidNumberException {
        Integer result = MagicConstant.find("444");
    }

    @Test(expected = InvalidNumberException.class)
    public void invalidInputAllDigitsAreTheSame() throws InvalidNumberException {
        Integer result = MagicConstant.find("4444");
    }

    @Test
    public void maxAndMinStepsToConverge() throws InvalidNumberException {
        Map<Integer, Integer> steps = new HashMap();
        for(int i = 0; i< 10000; i++){
            MagicConstant mc = new MagicConstant(i);
            Integer result = mc.find();
            if(result == 6174){
                if(steps.containsKey(mc.getNumberOfCalls()))
                    steps.put(mc.getNumberOfCalls(), steps.get(mc.getNumberOfCalls())+1);
                else
                    steps.put(mc.getNumberOfCalls(), 1);
            }
        }
        System.out.println(steps.toString());
    }
}
