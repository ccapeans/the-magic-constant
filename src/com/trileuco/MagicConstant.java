package com.trileuco;

import java.util.ArrayList;
import java.util.Collections;

public class MagicConstant {

    ArrayList<Integer> inputNumber;
    int smallestNumber;
    int biggestNumber;
    int numberOfCalls;

    public MagicConstant(String inputNumber) throws InvalidNumberException {
        this.inputNumber = this.split(inputNumber);

        if(!isValid())
            throw new InvalidNumberException();

        this.smallestNumber = this.getSmallest();
        this.biggestNumber = this.getBiggest();
        this.numberOfCalls = 0;
    }

    public MagicConstant(Integer inputNumber) throws InvalidNumberException {
        this.inputNumber = this.split(inputNumber);

        this.smallestNumber = this.getSmallest();
        this.biggestNumber = this.getBiggest();
        this.numberOfCalls = 0;
    }

    public int getNumberOfCalls() {
        return numberOfCalls;
    }

    public static Integer find(String number) throws InvalidNumberException {
        MagicConstant mc = new MagicConstant(number);
        return mc.find();
    }

    public Integer find(){
        int previousNumber = this.subtract();
        return findRecursive(previousNumber);
    }

    private Integer findRecursive(int previousNumber){
        this.numberOfCalls++;

        int newNumber = this.updateNumberWithPrevious(previousNumber);
        while(previousNumber != newNumber){
            previousNumber = newNumber;
            return findRecursive(previousNumber);
        }
        return newNumber;
    }

    private Integer subtract() {
        return this.biggestNumber - this.smallestNumber;
    }

    private Integer updateNumberWithPrevious(Integer previousNumber){
        this.inputNumber = this.split(Integer.toString(previousNumber));
        this.smallestNumber = this.getSmallest();
        this.biggestNumber = this.getBiggest();
        return this.subtract();
    }

    private boolean isValid() {
        if(this.inputNumber.size() != 4 || allDigitsAreTheSame())
            return false;
        return true;
    }

    private boolean allDigitsAreTheSame(){
        for(Integer i : this.inputNumber){
            for(Integer j : this.inputNumber){
                if (i != j)
                    return false;
            }
        }
        return true;
    }

    private ArrayList<Integer> split(String numbers){
        ArrayList result = new ArrayList();
        for(String s : numbers.split("")){
            result.add(Integer.parseInt(s));
        }
        // Retains zeros and ensures that the number has always 4 digits
        if(result.size() == 3)
            result.add(0);
        return result;
    }

    private ArrayList<Integer> split(Integer numbers){
        ArrayList result = new ArrayList();
        for(String s : Integer.toString(numbers).split("")){
            result.add(Integer.parseInt(s));
        }
        // Retains zeros and ensures that the number has always 4 digits
        while(result.size() < 4)
            result.add(0);
        return result;
    }

    private Integer getBiggest() {
        ArrayList<Integer> copyNumber = (ArrayList<Integer>)this.inputNumber.clone();
        Collections.sort(copyNumber);
        return convertArrayListToInteger(copyNumber);
    }

    private Integer getSmallest() {
        ArrayList<Integer> copyNumber = (ArrayList<Integer>)this.inputNumber.clone();
        Collections.sort(copyNumber, Collections.reverseOrder());
        return convertArrayListToInteger(copyNumber);
    }

    private static Integer convertArrayListToInteger(ArrayList<Integer> numbers){
        Integer result = 0;
        for (int i = numbers.size() - 1; i >= 0; --i) {
            result = 10*result + numbers.get(i);
        }
        return result;
    }
}
