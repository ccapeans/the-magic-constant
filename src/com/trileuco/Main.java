package com.trileuco;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

    public static void main(String[] args) throws IOException, InvalidNumberException {

        System.out.print("Write a 4 digits number (REMEMBER: All the digits can't be the same): ");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String number = reader.readLine();

        Integer result = MagicConstant.find(number);
        System.out.println("The magic constant is: " + result);
    }
}
